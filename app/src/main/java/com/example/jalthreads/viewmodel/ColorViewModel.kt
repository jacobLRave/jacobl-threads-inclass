package com.example.jalthreads.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.jalthreads.model.ColorRepo
import kotlinx.coroutines.launch

class ColorViewModel : ViewModel() {
    private val repo = ColorRepo

    private val _randomColor = MutableLiveData<Int>()
    val randomColor: LiveData<Int> get() = _randomColor

    fun getRandomColor() {
        viewModelScope.launch {
            val randomColor = repo.getRandomColor()
            _randomColor.value = randomColor
        }
    }

}