package com.example.jalthreads.model

import com.example.jalthreads.model.remote.ColorApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext

object ColorRepo {
    private val colorApi = object : ColorApi {
        override suspend fun randomColor(): Int {
            return randomColor
        }
    }

    suspend fun getRandomColor(): Int = withContext(Dispatchers.IO) {
        delay(2000)
        colorApi.randomColor()
    }
}