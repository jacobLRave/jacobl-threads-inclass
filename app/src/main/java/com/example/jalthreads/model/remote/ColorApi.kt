package com.example.jalthreads.model.remote

interface ColorApi {

   suspend fun randomColor(): Int

}