package com.example.jalthreads.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.jalthreads.R
import com.example.jalthreads.databinding.FragmentDisplayBinding

class DisplayFragment : Fragment() {
    private var _binding: FragmentDisplayBinding? = null
    private val binding get() = _binding!!
    private val args by navArgs<DisplayFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentDisplayBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.vBackground.setBackgroundColor(args.color)
//        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner){
//            val moveToSelectorDirection = DisplayFragmentDirections.actionDisplayFragmentToSelectorFragment()
//            findNavController().navigate(moveToSelectorDirection)
//        }

    }

}