package com.example.jalthreads.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.jalthreads.R
import com.example.jalthreads.databinding.FragmentSelectorBinding
import com.example.jalthreads.viewmodel.ColorViewModel


class SelectorFragment : Fragment() {

    private var _binding: FragmentSelectorBinding? = null
    private val binding get() = _binding!!
    private val colorViewModel by viewModels<ColorViewModel>()
    private var cachedColor = 0
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentSelectorBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        colorViewModel.randomColor.observe(viewLifecycleOwner) { color ->
            val colorDirection =
                SelectorFragmentDirections.actionSelectorFragmentToDisplayFragment(color)
            if(cachedColor != color) {
                cachedColor = color
                findNavController().navigate(colorDirection)
            }
        }

        binding.selectColor.setOnClickListener() {
            colorViewModel.getRandomColor()
        }
//        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner){
//            val toSelector = SelectorFragmentDirections.
//        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}